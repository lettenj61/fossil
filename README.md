Fossil
======

Write, fetch and share Scala apps / libs for [Ammonite][amm] using git hosting services.

## Installation

`fossil` itself is just a Scala script for Ammonite. All you need is to fetch [this script][fossil-main] and give its name to `amm` executable.

There's also wrapper scripts for [Windows][launcher-win] / [Unix-like][launcher-nix] platforms for convenience.

## Documentation

A quick start guide (WIP)

## About

Scripting with Scala became very easy & fun since Ammonite appeared, and I thought it would be more happier times when I get something like [conscript][cs] for Ammonite.

But full scaled __package manager__ seems to bit too heavy for my use cases, i.e. daily scripting with Scala, and most of the cases could have been covered with simple `git clone`.

`fossil` introduces some opinionated conventions in Ammonite's script dependency management. It also brings utilities to share executable scripts via git hosting services, like GitHub, GitLab, etc.

## Todo

* Rewrite file dependency when on installing global script
* Recursively resolve fossil package dependency

## License

MIT.

[amm]: http://ammonite.io/
[cs]: #
[fossil-main]: https://gitlab.com/lettenj61/fossil/blob/master/Fossil.sc
[launcher-win]: https://gitlab.com/lettenj61/fossil/blob/master/bin/fossil.cmd
[launcher-nix]: https://gitlab.com/lettenj61/fossil/blob/master/bin/fossil
