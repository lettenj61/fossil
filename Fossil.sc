import java.io.File
import java.nio.file.FileSystems
import java.net.URI
import scala.util.Properties.isWin
import scala.util.{ Try, Success, Failure }

import ammonite.{ ops => Ops }
import ammonite.ops.Path
import scopt.OptionParser
import upickle.{ default => pickle }

// ---- Models

/**
 * Shared configurations used in fossil and its subcommands.
 */
case class Options(
  subcommand: Option[String] = Some("get"),
  repo: String = "",
  host: String = "github.com",
  branch: Option[String] = None,
  dryRun: Boolean = false,
  global: Boolean = false,
  nameToInit: Option[String] = None
)

/**
 * Package model for fossil.
 */
case class PackageInfo(
  version: String,
  name: String,
  nodejs: Boolean = false,
  main: Map[String, String] = Map.empty,
  excludes: Seq[String] = Nil,
  packages: Map[String, String] = Map.empty
)
object PackageInfo {
  def defaultInDirectory(): PackageInfo =
    PackageInfo("1.0.0", Ops.pwd.name)
  implicit val readWriter: pickle.ReadWriter[PackageInfo]
    = pickle.macroRW[PackageInfo]
}

/**
 * Platform aware API which wraps `ammonite.ops.Command`.
 */
trait ShellWrapper {
  import ammonite.ops.{ Command, Shellout }

  protected type Reported = Either[Vector[String], String]

  def dryRun: Boolean

  def baseCommand: Vector[String] =
    if (isWin) Vector("cmd", "/c")
    else Vector.empty

  // Maybe we don't need `exec`
  lazy val exec = Command(baseCommand, Map.empty, Shellout.executeInteractive)
  lazy val execStream = Command(baseCommand, Map.empty, Shellout.executeStream)

  /**
   * Execute command then wraps result in Either.
   * On success, Right(captured) where `captured` is entire output from STDOUT
   * On failure, Left(lines) where `lines` is Vector[String] of each lines from STDERR
   *
   * Any non-zero exit codes are treated as failure.
   */
  protected def wrapEither(workDir: Path, cmd: String, args: Seq[String]): Reported = {
    if (dryRun) {
      Right(
        s"""
          |DIR: ${workDir}
          |COMMAND:
          |$cmd ${args.mkString(" ")}
        """.stripMargin.trim
      )
    } else {
      val result = execStream(cmd, args)(workDir)
      result.exitCode match {
        case 0 => Right(result.out.string)
        case nonzero => Left(result.err.lines)
      }
    }
  }
}

class GitOps(options: Options) extends ShellWrapper {
  val dryRun: Boolean = options.dryRun
  private def uriString: String = {
    val uri = new URI(options.repo)
    Option(uri.getScheme) match {
      case Some(v) => v match {
        case "git" | "http" | "https" | "file" =>
          uri.toString
        case _ =>
          throw new IllegalArgumentException(uri.toString)
      }
      case None =>
        s"https://${options.host}/${options.repo}"
    }
  }

  private def runGit(workDir: Path, gitArgs: Seq[String]): Reported =
    wrapEither(workDir, "git", gitArgs)

  // TODO: Consider not to force `pull` but attempt to `fetch` first.
  // TODO: Maybe select remote?
  def pull(workDir: Path): Reported =
    runGit(
      workDir,
      List("pull") ++ (options.branch match {
        case None => Nil
        case Some(b) => List("origin", s"$b:$b")
      })
    )

  def clone(workDir: Path): Reported =
    runGit(
      Ops.pwd,
      List("clone") ++ (options.branch match {
        case None => List(uriString, workDir.toString)
        case Some(b) => List("-b", b, uriString, workDir.toString)
      })
    )
}

class Installer(options: Options) {
  val installPath = 
    if (options.global) FileIO.findPackagePath(options)
    else Ops.tmp.dir(prefix = "fossil_")

  def install(): Unit = {
    // println(installPath)
    val destDir = FileIO.findPackagePath(options)
    if (Ops.exists(destDir/"fossil.json")) {
      println(s"Package ${options.repo} already installed")
    } else {
      (new GitOps(options).clone(installPath) match {
        case Left(messages) =>
          System.err.println(messages.mkString("\n"))
          None
        case Right(out) =>
          // println(out)
          Some(installPath)
      }).flatMap(p => FileIO.readPackageInfo(p) match {
        case Success(packageInfo) =>
          Some(packageInfo)
        case Failure(err) =>
          System.err.println(err.getMessage)
          None
      }).fold[Unit] {
        System.err.println(s"${options.host}/${options.repo} is not a fossil package")
        Ops.rm(installPath)
        System.exit(1)
      } { packageInfo =>
        if (packageInfo.main.size == 0) {
          // Here we need to copy library files
          val fs = FileSystems.getDefault
          val matchers = packageInfo.excludes.map { pat =>
            fs.getPathMatcher(s"glob:$pat")
          }
          Ops.mkdir(destDir)
          Ops.ls(installPath)
            .filter(f => f.isFile && f.name != "fossil.json" && {
              !matchers.exists(_.matches(f.toNIO))
            })
            .foreach(from => Ops.cp.into(from, destDir))
          Ops.rm(installPath)
          println("Successfully installed package: " + packageInfo.name)
        } else {
          packageInfo.main.foreach { case (binName, runner) =>
            val platformBin = FileIO.writeLauncherScript(options, packageInfo.nodejs, binName, runner)
            println("Successfully installed executable: " + platformBin)
          }
        }
      }
    }
  }
}

object FileIO {
  import ammonite.ops._

  def fossilHome: Path = home/"_fossil"
  def globalPackageDir: Path = fossilHome/"pkg"
  def globalBinDir: Path = fossilHome/"bin"

  def findPackageRoot(isGlobal: Boolean): Path =
    if (isGlobal) globalPackageDir else pwd/"pkg"

  def findPackagePath(options: Options): Path = {
    val base = findPackageRoot(options.global)
    val uri = new URI(options.repo)
    Option(uri.getScheme) match {
      case Some("file") =>
        val src = new File(uri)
        base/"localhost"/System.getProperty("user.name")/src.getName
      case None =>
        val Array(user, repo) = options.repo.split("/", 2)
        if (options.global) base/options.host/user/repo
        else base/repo
    }
  }

  def readPackageInfo(dirname: Path): Try[PackageInfo] = {
    val packageInfo = dirname/"fossil.json"
    if (exists(packageInfo)) {
      Try(read(packageInfo))
        .map(content => pickle.read[PackageInfo](content))
    } else Failure(new NoSuchElementException("could not find " + packageInfo.toString))
  }

  def writePackageInfo(dest: Path, packageInfo: PackageInfo): Unit = {
    for (parent <- Option(dest.toNIO.getParent)) {
      mkdir(Path(parent))
    }
    write.over(
      dest,
      pickle.write(packageInfo, indent = 2)
    )
  }

  def initBlankPackage(name: Option[String]): Unit = {
    val target = name match {
      case Some(p) => pwd/p
      case _ => pwd
    }
    val packageInfo = target/"fossil.json"
    if (exists(packageInfo)) {
      System.err.println(s"Error: [$packageInfo] already exists")
    } else {
      mkdir(target)
      writePackageInfo(target, PackageInfo("1.0.0", name.getOrElse(target.name)))
      println("Successfully created: " + packageInfo)
    }
  }

  def showBinDirectory(options: Options): Unit = {
    val binDir = if (options.global) globalBinDir else pwd/"bin"
    println(binDir)
  }

  def listPackages(options: Options): Unit = {
    val p = findPackageRoot(options.global)
    val installedPackages =
      if (exists(p)) {
        ls.rec(p)
          .withFilter(f => f.isDir && exists(f/"fossil.json"))
          .flatMap { s =>
            s.name :: List {
              readPackageInfo(s)
                .get.main.keys.toSeq
                .sortBy(s => s)
                .mkString("  with binaries(", ", ", ")")
            }
          }
          .toList
      } else Nil
    installedPackages match {
      case Nil =>
        val parent = p.toNIO.getParent.toString
        System.err.println(s"No package is installed under $parent")
      case xs =>
        println("installed packages:")
        xs.foreach(println)
    }
  }

  def launcherScript(options: Options, isNodeJS: Boolean, runner: String): String = {
    val cmdName = if (isNodeJS) "node" else "amm"
    val Array(user, repo) = options.repo.split("/", 2)
    if (isWin) {
      s"""
        |@echo off
        |setlocal EnableDelayedExpansion
        |
        |$cmdName "%~dp0\\..\\pkg\\${options.host}\\$user\\$repo\\$runner" %*
      """.stripMargin.trim
    } else {
      val vararg = "$@"
      s"""
        |#!/bin/bash
        |
        |$cmdName ../pkg/${options.host}/$user/$repo/$runner $vararg
      """.stripMargin.trim
    }
  }

  def writeLauncherScript(
    options: Options,
    isNodeJS: Boolean,
    binName: String,
    runner: String
  ): String = {
    val platformBin = globalBinDir/(if (isWin) s"$binName.cmd" else binName)
    write.over(
      platformBin,
      launcherScript(options, isNodeJS, runner)
    )
    platformBin.toString
  }
}

// ---- Constants.
val VERSION: String = "0.1.0"

/** The option parser */
val parser: OptionParser[Options] = new OptionParser[Options]("fossil") {
  head("fossil", VERSION)

  help("help")
    .abbr("h")
    .text("print this help message")

  opt[String]('b', "branch")
    .valueName("[name]")
    .action((b, c) => c.copy(branch = Some(b)))
    .text("branch name of the package")

  opt[Unit]('g', "global")
    .action((_, c) => c.copy(global = true))
    .text("install package globally")

  opt[Unit]('D', "dry-run")
    .hidden()
    .action((_, c) => c.copy(dryRun = true))
    .text("report command to execute without actually invoke them")

  opt[String]('H', "host")
    .valueName("[name]")
    .action((host, c) => c.copy(host = host))
    .text("name of git host (default: github.com)")

  // default argument captures, for the subcommand implicitly set to "get"
  arg[String]("<repo>")
    .optional()
    .hidden()
    .action((repo, c) => c.copy(repo = repo))

  // Separate command help with newline
  note("")

  cmd("get")
    .action((_, c) => c.copy(subcommand = Some("get"))) // Maybe we could pass it?
    .children(
      arg[String]("<repo>")
        .required()
        .action((repo, c) => c.copy(repo = repo))
        .text("package name in 'user/repo' format")
    )
    .text("fetch specified package")

  cmd("init")
    .action((_, c) => c.copy(subcommand = Some("init")))
    .children(
      arg[String]("name")
        .optional()
        .action((n, c) => c.copy(nameToInit = Some(n)))
        .text("name of the new package")
    )
    .text("initialize empty fossil package")

  cmd("update")
    .action((_, c) => c.copy(subcommand = Some("update")))
    .text("update package for newer version if there is any")

  cmd("list")
    .action((_, c) => c.copy(subcommand = Some("list")))
    .text("list installed packages")

  cmd("bin")
    .action((_, c) => c.copy(subcommand = Some("bin")))
    .text("print `bin` directory")
}

// ---- Run application.
@main
def run(args: String*): Unit = parser.parse(args, Options()) match {
  // FIXME make it readable
  case Some(options) =>
    options.subcommand.fold[Unit] {
      parser.showUsageAsError()
    } {
      case "init" =>
        FileIO.initBlankPackage(options.nameToInit)
      case "get" =>
        (new Installer(options)).install()
      case "list" =>
        FileIO.listPackages(options)
      case "bin" =>
        FileIO.showBinDirectory(options)
      case "update" =>
        println("Subcommand [" + options.subcommand.get + "] is not implemented")
        System.exit(1)
      case cmd =>
        System.err.println(s"Unknown command: $cmd")
        parser.showUsageAsError()
    }
  case _ => parser.showUsageAsError()
}
